# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.

import hashlib
import tarfile
from StringIO import StringIO

class RisnoSlurpPipeline(object):
    def __init__(self):
        self.tar = {}

    def process_item(self, item, spider):
        if item['html']:
            html = item['html']
            del item['html']
            file_name = hashlib.sha224(item['url']).hexdigest() + '.html'
            item['file_name'] = file_name
            self._to_tar(spider, file_name, html)
        return item

    def open_spider(self, spider):
        if self._should_store_html(spider):
            spider.logger.info('Storing data to %s', spider.tarfile)
            self.tar[spider] = tarfile.open(spider.tarfile, "w:bz2")
        else:
            spider.logger.warning('data not stored. Please specify a .tar.bzip2 file to store to if you need with -a tarfile=leboncoin.tar.bzip2')

    def close_spider(self, spider):
        if self._should_store_html(spider):
            self.tar[spider].close()

    def _to_tar(self, spider, name, content):
        if self._should_store_html(spider):
            the_content = StringIO(content)
            info = tarfile.TarInfo(name=name)
            info.size = len(the_content.buf)

            self.tar[spider].addfile(tarinfo=info, fileobj=the_content)

    def _should_store_html(self, spider):
        return hasattr(spider, 'tarfile')
