# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.

import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

class SudouestSpider(CrawlSpider):
    #TODO filter offers without image
    name = 'sudouest'
    allowed_domains = ['sudouest-annonces.com']
    start_urls = ['http://www.sudouest-annonces.com/annonce/liste.html?rub=IMO003']

    rules = (
        Rule(LinkExtractor(restrict_css='a.next')),
        Rule(LinkExtractor(restrict_css='.annonce.liste a'),
                           callback='parse_offer'),
    )

    def parse_offer(self, response):
        return {
                'html': response.body,
                'url': response.url,
                'title': response.css('#fiche_annonce h1').extract_first().strip(),
                'img': response.urljoin(response.css('#photos_annonce img::attr(src)').extract_first()),
        }
