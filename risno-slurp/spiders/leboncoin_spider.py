# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.

import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

class LeboncoinSpider(CrawlSpider):
    #TODO filter offers without image
    name = 'leboncoin'
    allowed_domains = ['leboncoin.fr']
    start_urls = ['https://www.leboncoin.fr/ventes_immobilieres/offres/']

    rules = (
        Rule(LinkExtractor(restrict_css='a[id=next]')),
        Rule(LinkExtractor(restrict_css='li[itemtype="http://schema.org/Offer"]'),
                           callback='parse_offer'),
    )

    def parse_offer(self, response):
        return {
                'html': response.body,
                'url': response.url,
                'title': response.css('title::text').extract_first().strip(),
                'img': response.urljoin(response.css('.lazyload::attr(data-imgsrc)').extract_first()),
        }
